import UIKit
import CoreData


class AddVC: UIViewController {

   
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var sub1: UITextField!
    @IBOutlet weak var sub3: UITextField!
    @IBOutlet weak var sub4: UITextField!
    
    @IBOutlet weak var sub5: UITextField!
    @IBOutlet weak var sub2: UITextField!
    let appDelegateObj : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var editRecNo = Int()
    var dataArray = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if editRecNo != -1 {
            txtName.text = self.dataArray[editRecNo].value(forKey: kNameStr) as? String
            txtPhone.text = self.dataArray[editRecNo].value(forKey: gpa) as? String
        }
    }
    
    @IBAction func btnDoneTapped(_ sender: AnyObject) {
        var s1 = Float(sub1.text!);
        var s2 = Float(sub2.text!);
        var s3 = Float(sub3.text!);
        var s4 = Float(sub4.text!);
        var s5 = Float(sub5.text!);
        let totalGpa:Float = s1!+s2!+s3!+s4!+s5!;
        let avgGpa = totalGpa/5;
        print("surinderrrrr")
        print(avgGpa);
        if editRecNo != -1 {
            self.dataArray[editRecNo].setValue(txtName.text!, forKey: kNameStr)
            
            self.dataArray[editRecNo].setValue(avgGpa, forKey: gpa)
           
            
            do {
                try self.dataArray[editRecNo].managedObjectContext?.save()
            } catch {
                print("Error occured during updating entity")
            }
        } else {
            let entityDescription = NSEntityDescription.entity(forEntityName: kEntityStr, in: appDelegateObj.managedObjectContext)
            let newPerson = NSManagedObject(entity: entityDescription!, insertInto: appDelegateObj.managedObjectContext)
            newPerson.setValue(txtName.text!, forKey: kNameStr)
            newPerson.setValue(avgGpa, forKey: gpa)
            
            do {
                try newPerson.managedObjectContext?.save()
            } catch {
                print("Error occured during save entity")
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelTappe(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

}

